<?php

define("ACCESS_TOKEN","eb5103bb-e5da-4718-b7e7-33a767276191");

error_log("Deploying");
error_log($_GET['access_token']);
error_log($_GET['deployment_path']);
error_log($_GET['artifact_url']);

// validate access token
if($_GET['access_token'] != ACCESS_TOKEN){
 echo "Authentication Failure";
 exit();
}


// check if directory exists
$deployment_path=$_GET['deployment_path'];
error_log('Current Deployment Path: '.$deployment_path);
if ( !file_exists( $deployment_path ) && !is_dir( $deployment_path ) ) {
    // make directory
    mkdir( $deployment_path );
}
// change directory
chdir($deployment_path);

// log current directory
error_log('Current Deployment Directory: '.getcwd());

// artifact url
$artifact_url=$_GET['artifact_url'];
// get file content
$artifact_file = file_get_contents($artifact_url, true);
// put file content
file_put_contents('../temp/artifact.zip',$artifact_file);